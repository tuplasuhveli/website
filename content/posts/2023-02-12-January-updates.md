---
title: January 2023 updates
date: 2023-02-12 16:56:54
categories: verein
keywords:
---

- First round of new members joined! **yajo** and the [Institute for Common Good Technology](https://commongoodtechnology.org) (ICGT) represented by its president **sebix**
- The first elections required for full functional association happened on January 12th (TODO link to results)
- Sailmates also joins ICGT as member and ICGT supports Sailmates with the website and e-mail.
- **poetaster** joins the association. Poetaster made funding applications for [keypeer.org](keypeer.org) and would like the project management to be in a non-profit association.